//
//  PaymentManager.h
//  ZCJZ
//
//  Created by sujeking on 2019/3/1.
//  Copyright © 2019年 jeking. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlipaySDK/AlipaySDK.h"
#import "WXApi.h"
#import "WWKApi.h"


@protocol WXApiManagerDelegate <NSObject>

@optional
- (void)managerDidRecvPaymentResponse:(PayResp *)response;
- (void)managerDidRecvMiniResponse:(WXLaunchMiniProgramResp *)response;
- (void)managerDidRecvShareResponse:(SendMessageToWXResp *)response;
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response;
@end

@interface PaymentManager : NSObject <WXApiDelegate,WWKApiDelegate>

@property (nonatomic, weak) id<WXApiManagerDelegate> delegate;

- (void)setAliPayCallback:(NSDictionary *)info;

+ (instancetype)sharedManager;

- (void)weiXinPayWithDic:(NSDictionary *)wechatPayDic;

- (void)loadWeiXinMiniProgram:(NSString *)orderId;

/// 小程序充值页面
- (void)loadWeiXinMiniProgram2Recharge;
/*
 NSString *maintitle = arr[0];
 NSString *subtitle = arr[1];
 NSString *thumbimvurl = arr[2];
 NSString *path = arr[3];
 NSString *url = arr[4];
 */
/// 分享给微信好友
- (void)share2WeiXinFirendList:(id)obj;
/*
NSString *maintitle = arr[0];
NSString *subtitle = arr[1];
NSString *thumbimvurl = arr[2];
NSString *path = arr[3];
NSString *url = arr[4];
*/
/// 分享小程序到好友
- (void)share2MiniProgramFirendList:(id)obj;
/*
NSString *maintitle = arr[0];
NSString *subtitle = arr[1];
NSString *thumbimvurl = arr[2];
NSString *path = arr[3];
NSString *url = arr[4];
*/
/// 分享到朋友圈
- (void)share2WeiXinCycle:(id)obj;

/// 微信授权登录
- (void)auth2WeiXinWithVC:(UIViewController *)vc callback:(void(^)(NSString *msg))callback;
/// 登录
- (void)login2WeiXinWithCallback:(void(^)(NSString *msg))callback;
/// 微信支付
- (void)loadWXPayWithWithOrderSignInfo:(NSDictionary *)info;
/// 小程序支付
- (void)loadWeiXinMiniProgram4Pay:(NSDictionary *)data;
/// 支付宝支付
- (void)loginAliPayWithOrderSignStr:(NSString *)str callback:(void(^)(NSDictionary *dict))callback;

/// 小程序直播
//-(void)loadWeiXinMiniProgramLivingWithData:(NSDictionary *)data;
-(void)loadWeiXinMiniProgramLivingWithRoom_id:(NSString *)room_id;
/// 进入小程序的看点直播中专页面
-(void)loadWeiXinMiniProgramLivingWithRoom_idForKanDian:(NSString *)room_id;

/// 进入小程序
- (void)loadWeiXinMiniProgramWithPath:(NSString *)path;


@end

