//
//  CDPayModule.h
//  CDPayModule
//
//  Created by sujeking on 2022/7/30.
//

#import <Foundation/Foundation.h>

//! Project version number for CDPayModule.
FOUNDATION_EXPORT double CDPayModuleVersionNumber;

//! Project version string for CDPayModule.
FOUNDATION_EXPORT const unsigned char CDPayModuleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CDPayModule/PublicHeader.h>

#import <CDPayModule/PaymentManager.h>
