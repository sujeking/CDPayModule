//
//  PaymentManager.m
//  ZCJZ
//
//  Created by sujeking on 2019/3/1.
//  Copyright © 2019年 jeking. All rights reserved.
//

#import "PaymentManager.h"
#import "WXApiObject.h"

NSString *wxAppName = @"gh_79dd5bd44d4c";

@interface PaymentManager()

@property (assign, nonatomic) WXMiniProgramType type;

@property (nonatomic, copy) void(^callback)(id info);

@end


@implementation PaymentManager

+ (instancetype)sharedManager {
    static PaymentManager *obj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        obj = [[PaymentManager alloc] init];
#if DEBUG
//         环境配置
        obj.type = WXMiniProgramTypePreview;
#else
        obj.type = WXMiniProgramTypeRelease;
#endif
    });
    return obj;
}

// MARK: - WXApiDelegate

- (void)weiXinPayWithDic:(NSDictionary *)wechatPayDic {
    PayReq *req = [[PayReq alloc] init];
    
    req.openID = [wechatPayDic objectForKey:@"appId"];
    req.timeStamp = [[wechatPayDic objectForKey:@"timeStamp"] intValue];
    req.package = [wechatPayDic objectForKey:@"packages"];
    req.nonceStr = [wechatPayDic objectForKey:@"nonceStr"];
    req.sign = [wechatPayDic objectForKey:@"paySign"];
    
    req.partnerId = [wechatPayDic objectForKey:@"partnerId"];
    req.prepayId = [wechatPayDic objectForKey:@"prepayId"];
    
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
    [WXApi openWXApp];
}

- (void)onResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[PayResp class]]) { //支付回调
        PayResp *response = (PayResp *)resp;
        if (_delegate && [_delegate respondsToSelector:@selector(managerDidRecvPaymentResponse:)]) {
            [_delegate managerDidRecvPaymentResponse:response];
        }
    }
    if ([resp isKindOfClass:[WXLaunchMiniProgramResp class]]) { //小程序回调
        WXLaunchMiniProgramResp *res = (WXLaunchMiniProgramResp *)resp;
        if (_delegate && [_delegate respondsToSelector:@selector(managerDidRecvMiniResponse:)]) {
            [_delegate managerDidRecvMiniResponse:res];
        }
    }

    if ([resp isKindOfClass:[SendMessageToWXResp class]]) { //分享回调
        SendMessageToWXResp *res = (SendMessageToWXResp *)resp;
        if (_delegate && [_delegate respondsToSelector:@selector(managerDidRecvShareResponse:)]) {
            [_delegate managerDidRecvShareResponse:res];
        }
    }

    if ([resp isKindOfClass:[SendAuthResp class]]) { //分享回调
        SendAuthResp *res = (SendAuthResp *)resp;
        if (_delegate && [_delegate respondsToSelector:@selector(managerDidRecvAuthResponse:)]) {
            [_delegate managerDidRecvAuthResponse:res];
        }
    }
    
    /* 企业微信-选择联系人的回调 */
    if ([resp isKindOfClass:[WXOpenCustomerServiceResp class]])
    {
        WXOpenCustomerServiceResp *res = (WXOpenCustomerServiceResp *)resp;
        int errCode = res.errCode;        // 0 为成功，其余为失败
         NSString *string = res.extMsg;    // 相关错误信息
    }

}

- (void)loadWeiXinMiniProgram:(NSString *)orderId {
    WXLaunchMiniProgramReq *req = [WXLaunchMiniProgramReq object];
    req.userName = wxAppName;
//    ZCBaseUserManager *manager = [ZCBaseUserManager shareInstance];
//    ZCBaseUserModel *userModel = manager.userModel;
//    req.path = [NSString stringWithFormat:@"pages/pay/pay?order_id=%@&isApp=1&token=%@&uid=%@&from=app",
//                orderId,userModel.member.accessToken,
//                userModel.member.idstr];
    req.path = @"szw";
    req.miniProgramType = self.type;
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
}

- (void)loadWeiXinMiniProgram2Recharge {
    WXLaunchMiniProgramReq *req = [WXLaunchMiniProgramReq object];
    req.userName = wxAppName;
//    ZCBaseUserManager *manager = [ZCBaseUserManager shareInstance];
//    ZCBaseUserModel *userModel = manager.userModel;
//    req.path = [NSString stringWithFormat:@"pages/Earnings/topUp/topUp?isApp=1&token=%@&uid=%@&from=app",
//                userModel.member.accessToken,
//                userModel.member.idstr];
    req.path = @"szw";
    req.miniProgramType = self.type;
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
}

/// 分享给微信好友
- (void)share2WeiXinFirendList:(id)obj {
    WXMediaMessage *message = [WXMediaMessage message];
    if ([obj isKindOfClass:[UIImage class]]) {
        WXImageObject *imgobj = [WXImageObject object];
        imgobj.imageData = UIImageJPEGRepresentation(obj, 0.5);
        message.mediaObject = imgobj;
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.scene = WXSceneSession;
        req.message = message;
        req.bText = NO;
        [WXApi sendReq:req completion:^(BOOL success) {
            NSLog(@"%s %d",__func__ , success);
        }];//发送对象实例
    }
    if ([obj isKindOfClass:[NSArray class]]) {
        NSArray *arr = (NSArray *)obj;
        NSString *maintitle = arr.firstObject;
        NSString *subtitle = arr[1];
        NSString *thumbimvurl = arr[2];
        NSString *url = arr.lastObject;
        
        
        NSData *imvdata;
        if([thumbimvurl isKindOfClass:[NSData class]]) {
            imvdata = (NSData *)thumbimvurl;
        } else if ([thumbimvurl isKindOfClass:[NSString class]]) {
            imvdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbimvurl]];
        }
        UIImage *img = [UIImage imageWithData:imvdata];
//        img = [img rescaleImageToSize:(CGSize){200,200}];
        if (UIImageJPEGRepresentation(img,1.0).length > 64 * 1024) { //.length 一定大于imvdata.length  暂时未知原因
            img = [UIImage imageWithData:UIImageJPEGRepresentation(img, 0.5)];
        }
        
//        img = [img rescaleImageToSize:(CGSize){200,200}];
//        long imglength = UIImageJPEGRepresentation(img,1.0).length;
//        NSArray *arrScale = @[@(0.8),@(0.5),@(0.3),@(0.1)];
//        NSInteger indexScale = 0;
//        while (imglength >= 64*1024 && indexScale < arrScale.count) {
//            CGFloat scale = [arrScale[indexScale] floatValue];
//            img = [UIImage imageWithData:UIImageJPEGRepresentation(img, scale)];
//            imglength = UIImageJPEGRepresentation(img,1.0).length;
//            indexScale += 1;
//        }

        message.title = maintitle;
        message.description = subtitle;
        [message setThumbImage:img];
        
        WXWebpageObject *webo = [WXWebpageObject object];
        webo.webpageUrl = url;
        message.mediaObject = webo;
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.scene = WXSceneSession;
        req.message = message;
        req.bText = NO;
        [WXApi startLogByLevel:WXLogLevelDetail logBlock:^(NSString * _Nonnull log) {
            NSLog(@"-----------------------");
            NSLog(@"%@",log);
            NSLog(@"-----------------------");
        }];

        [WXApi sendReq:req completion:^(BOOL success) {
            NSLog(@"%s %d",__func__ , success);

        }];//发送对象实例
    }
}
/// 分享到朋友圈
- (void)share2WeiXinCycle:(id)obj {
    WXMediaMessage *message = [WXMediaMessage message];
    if ([obj isKindOfClass:[UIImage class]]) {
        WXImageObject *imgobj = [WXImageObject object];
        imgobj.imageData = UIImageJPEGRepresentation(obj, 0.5);
        message.mediaObject = imgobj;
    }
    if ([obj isKindOfClass:[NSArray class]]) { //数组来分享对象【标题、媒体地址】
        NSArray *arr = (NSArray *)obj;
        NSString *maintitle = arr.firstObject;
        id obj = arr.lastObject;
        if ([obj isKindOfClass:[NSString class]]) {
            NSString *txt = [NSString stringWithFormat:@"%@",obj];
            if ([txt hasPrefix:@"video://"]) {
                WXVideoObject *videobj = [WXVideoObject object];
                videobj.videoUrl = [NSString stringWithFormat:@"%@",[txt substringFromIndex:8]];
                message.mediaObject = videobj;
                message.title = maintitle;
            }
            if ([txt hasPrefix:@"http://"]||[txt hasPrefix:@"https://"]) {
                WXWebpageObject *webo = [WXWebpageObject object];
                webo.webpageUrl = txt;
                message.mediaObject = webo;
                UIImage *img = [UIImage imageNamed:@"AppIcon"];
                NSString *thumbimvurl = arr[2];
                if (thumbimvurl.length != 0) {
                    NSData *imvdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbimvurl]];
                    img = [UIImage imageWithData:imvdata];
                }
                
//                img = [img rescaleImageToSize:(CGSize){200,200}];
                if (UIImageJPEGRepresentation(img,1.0).length > 64 * 1024) { //.length 一定大于imvdata.length  暂时未知原因
                    img = [UIImage imageWithData:UIImageJPEGRepresentation(img, 0.5)];
                }
                
//                img = [img rescaleImageToSize:(CGSize){200,200}];
//                long imglength = UIImageJPEGRepresentation(img,1.0).length;
//                NSArray *arrScale = @[@(0.8),@(0.5),@(0.3),@(0.1)];
//                NSInteger indexScale = 0;
//                while (imglength >= 64*1024 && indexScale < arrScale.count) {
//                    CGFloat scale = [arrScale[indexScale] floatValue];
//                    img = [UIImage imageWithData:UIImageJPEGRepresentation(img, scale)];
//                    imglength = UIImageJPEGRepresentation(img,1.0).length;
//                    indexScale += 1;
//                }
                
                [message setThumbImage:img];
                message.title = maintitle;
                SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
                req.scene = WXSceneSession;
                req.message = message;
                req.bText = NO;
            }
        }
    }
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.scene = WXSceneTimeline;
    req.message = message;
    req.bText = NO;
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];//发送对象实例
}
/// 认证
- (void)auth2WeiXinWithVC:(UIViewController *)vc callback:(void(^)(NSString *msg))callback {
    if ([WXApi isWXAppInstalled]) {
        SendAuthReq *req = [[SendAuthReq alloc] init];
        req.scope = @"snsapi_userinfo";
        req.state = @"wzgios";
        [WXApi sendReq:req completion:^(BOOL success) {
            NSLog(@"%s %d",__func__ , success);
        }];
    } else {
        callback(@"你还没有安装微信");
    }
}

- (void)login2WeiXinWithCallback:(void(^)(NSString *msg))callback {
    if ([WXApi isWXAppInstalled]) {
        SendAuthReq *req = [[SendAuthReq alloc] init];
        req.scope = @"snsapi_userinfo";
        req.state = @"wzgios";
        [WXApi sendReq:req completion:^(BOOL success) {
            NSLog(@"%s %d",__func__ , success);
        }];
    } else {
        callback(@"你还没有安装微信");
    }
}

- (void)loginAliPayWithOrderSignStr:(NSString *)str callback:(void(^)(NSDictionary *dict))callback {
    self.callback = callback;
    [[AlipaySDK defaultService] payOrder:str fromScheme:@"mall.weizhegou.shop" callback:callback];
}

- (void)setAliPayCallback:(NSDictionary *)info {
    if (self.callback) {
        self.callback(info);
    }
}

- (void)loadWXPayWithWithOrderSignInfo:(NSDictionary *)info {
    PayReq *req = [[PayReq alloc] init];
    req.partnerId = [info valueForKey:@"partnerid"];
    req.prepayId = [info valueForKey:@"prepayid"];
    req.nonceStr = [info valueForKey:@"noncestr"];
    req.timeStamp = [[info valueForKey:@"timestamp"] intValue];
    req.sign = [info valueForKey:@"sign"];
    req.package = [info valueForKey:@"package"];
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
}

- (void)loadWeiXinMiniProgram4Pay:(NSDictionary *)data {
    WXLaunchMiniProgramReq *req = [WXLaunchMiniProgramReq object];
    NSString *strMiniPath = [data valueForKey:@"mini_path"];
    req.userName = [data valueForKey:@"original_id"];
    req.path = strMiniPath;
    req.miniProgramType = self.type;
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
}

- (void)share2MiniProgramFirendList:(id)obj {
    if ([obj isKindOfClass:[NSArray class]]) {
        NSArray *arr = (NSArray *)obj;
        NSString *maintitle = arr[0];
        NSString *subtitle = arr[1];
        NSString *thumbimvurl = arr[2];
        NSString *path = arr[3];
        NSString *url = arr[4];
        if (path == nil || path.length == 0) {
//            UIWindow *window = [UIApplication sharedApplication].keyWindow;
//            [window makeToast:@"小程序路劲有误" duration:2.0 position:CSToastPositionCenter];
            NSAssert(NO, @"小程序路劲有误");
            return;
        }
        
        WXMiniProgramObject *object = [WXMiniProgramObject object];
        object.webpageUrl = url;
        object.userName = @"gh_cc0a3b6a22b2";
        object.path = path;
        
        NSData *imvdata;
        if([thumbimvurl isKindOfClass:[NSData class]]) {
            imvdata = (NSData *)thumbimvurl;
        } else if ([thumbimvurl isKindOfClass:[NSString class]]) {
            imvdata = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbimvurl]];
        }
        UIImage *img = [UIImage imageWithData:imvdata];
        object.hdImageData =  UIImageJPEGRepresentation(img, 0.001);
        object.withShareTicket = NO;
        object.miniProgramType = self.type;
        WXMediaMessage *message = [WXMediaMessage message];
        message.title = maintitle;
        message.description =subtitle;
        message.thumbData = nil;  //兼容旧版本节点的图片，小于32KB，新版本优先
        //使用WXMiniProgramObject的hdImageData属性
        message.mediaObject = object;
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.message = message;
        req.scene = WXSceneSession;  //目前只支持会话
        [WXApi sendReq:req completion:^(BOOL success) {
            
        }];
    }
    
    
    
}

/// 小程序直播
-(void)loadWeiXinMiniProgramLivingWithRoom_id:(NSString *)room_id
{
    WXLaunchMiniProgramReq *req = [WXLaunchMiniProgramReq object];
//    NSString *strMiniPath = [data valueForKey:@"mini_path"];
//    NSString *userName = [data valueForKey:@"original_id"];
//    NSString *room_id = [data valueForKey:@"room_id"];
    //    room_id = @"11";
    NSString *userName = @"gh_cc0a3b6a22b2";
    NSString *userID = @"0";
//    if([ZCBaseUserManager shareInstance].userModel != nil) {
//        userID = [ZCBaseUserManager shareInstance].userModel.member.idstr;
//    }
    req.userName = userName;
    req.path = [NSString stringWithFormat:@"plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=%@&rid=%@",room_id,userID];
    req.miniProgramType = self.type;
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
}

/// 进入小程序的看点直播中专页面
-(void)loadWeiXinMiniProgramLivingWithRoom_idForKanDian:(NSString *)room_id;
{
    WXLaunchMiniProgramReq *req = [WXLaunchMiniProgramReq object];
    NSString *userName = wxAppName;
    NSString *userID = @"0";
//    if([ZCBaseUserManager shareInstance].userModel != nil) {
//        userID = [ZCBaseUserManager shareInstance].userModel.member.idstr;
//    }
    req.userName = userName;
    req.path = [NSString stringWithFormat:@"pages/liveHub/liveHub?room_id=%@",room_id];
    req.miniProgramType = self.type;
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
}


- (void)loadWeiXinMiniProgramWithPath:(NSString *)path {
    WXLaunchMiniProgramReq *req = [WXLaunchMiniProgramReq object];
    NSString *userName = @"gh_cc0a3b6a22b2";
    NSString *userID = @"0";
//    if([ZCBaseUserManager shareInstance].userModel != nil) {
//        userID = [ZCBaseUserManager shareInstance].userModel.member.idstr;
//    }
    req.userName = userName;
    req.path = path;
    req.miniProgramType = self.type;
    [WXApi sendReq:req completion:^(BOOL success) {
        NSLog(@"%s %d",__func__ , success);
    }];
}

- (UIViewController *)visibleViewController {
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;;
    return [self getVisibleViewControllerFrom:rootViewController];
}

- (UIViewController *)getVisibleViewControllerFrom:(UIViewController *) vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if (vc.presentedViewController) {
            return [self getVisibleViewControllerFrom:vc.presentedViewController];
        } else {
            return vc;
        }
    }
}

@end
